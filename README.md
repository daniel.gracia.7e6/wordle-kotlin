# Wordle Kotlin

## Índice  
1. [Introducion](#introducion)
2. [Como Jugar](#jugar)
3. [Funcionalidades](#funcionalidades)
4. [Lo que he aprendido](#aprendido)
5. [Cosas a mejorar](#mejoras)
---
## Introducion<a name="introducion"></a>
Wordle es un juego que consiste en adivinar una palabra de 5 letras

1. Clona el repositorio
```console
https://gitlab.com/daniel.gracia.7e6/wordle-kotlin.git
```

2. Abrelo en tu compilador de Kotlin favorito. (Ej. VSCode)
```console
code wordle-kotlin/
```

3. Ejecuta el programa y empieza a jugar. 
```console
** WORDLE **
-- TABLERO --
 -  -  -  -  - 
 -  -  -  -  - 
 -  -  -  -  - 
 -  -  -  -  - 
 -  -  -  -  - 
 -  -  -  -  - 
Introduce una palabra (max 5 caracteres): 
```

---
## Como Jugar<a name="jugar"></a>

Es sencillo, tienes que escribir una palabra y ver las letras que has acertado, que tendrán diferente color dependiendo de si acertaste:

- Si la letra aparece en **verde**, es porque la has acertado y está en la palabra, y también está en la casilla correcta de la palabra.


- Si la letra aparece en **amarillo**, es porque está en la palabra, pero no está en la casilla correcta.


- Si la letra aparece en **gris** es porque no has acertado, y no está en la palabra que tienes que adivinar.

---
## Funcionalidades<a name="funcionalidades"></a>
El juego utiliza un diccionario en Español que contiene todas las palabras de 5 letras sin accentos
Las palabras se pueden introducir tanto en mayuscula como en minuscula

---
## Lo que he aprendido<a name="aprendido"></a>
Hacer este programa me a permitido aprender sobre diferentes aspectos de la sintaxis de kotlin. Entre los más importantes se encuentran:
- Gestión de arrays y diccionarios
- Formatear el output con colores
- Bucles anidados

También he aprendido a hacer uso de comandos de bash para poder buscar palabras con 5 caracteres en un diccionario en español, asi como formatearlas para poder utilizarlas como un array en kotlin.

---
## Cosas a mejorar<a name="mejoras"></a>
1. Multiples apariciones de una misma letra
2. Codificación para poder utilizar accentos
3. Optimización de ciertas funcionalidades con el uso de funciones
